" vim-plug {{{
call plug#begin('~/.local/share/nvim/plugged') " initialize vim-plug (plugin manager)

" appearance {{{
Plug 'arcticicestudio/nord-vim', { 'branch': 'develop' } " nord theme
Plug 'itchyny/lightline.vim' " vim-airline
Plug 'sheerun/vim-polyglot' " more syntax highlighting
" }}}

" fuzzy finder {{{
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " fzf
Plug 'junegunn/fzf.vim' " more fzf (fuzzy finder)
" }}}

" distraction-free {{{
Plug 'junegunn/goyo.vim' " distraction-free writing
Plug 'junegunn/limelight.vim' " highlight current line
" }}}

" features {{{
Plug 'tpope/vim-commentary' " comment line
Plug 'godlygeek/tabular' " markdown
Plug 'plasticboy/vim-markdown' " markdown
Plug 'mhinz/vim-signify' " vcs status in gutter
" }}}

call plug#end() " end vim-plug block
" }}}

" tab width & shift {{{
set shiftwidth=4 " set tab width
set softtabstop=4 " set tab width
set expandtab " expand tabs to spaces
set shiftround " round to nearest multiple of shiftwidth
" }}}

" look and feel {{{
set termguicolors " use right colours
set number " line numbers
set relativenumber " use relative line numbers
set cursorline " highlight current line
set updatetime=250 " set update time to 250ms for gitgutter
set noshowmode " don't show mode on command line (in statusbar)
let g:vim_markdown_folding_disabled = 1 " don't fold sections in .md files
set foldmethod=marker " folding method
" }}}

" colours {{{
colorscheme nord " set colour scheme
let g:lightline = { 'colorscheme': 'nord' } " lightline colour scheme
" }}}

" omnicomplete {{{
filetype plugin on " enable filetype plugins
set omnifunc=syntaxcomplete#Complete " enable omnicomplete
" }}}

" plugins {{{
let g:vimwiki_list = [{'path': '~/worldbuilding/', 'syntax': 'markdown', 'ext': '.md'}] " vimwiki list
" }}}

"""""""""""""
" REMAPPING " {{{
"""""""""""""

let mapleader = "," " change leader key to ,

" vimagit toggle (,g)
nnoremap <Leader>g :Magit<CR>

" fzf file/buffer list (,p / ,f)
nnoremap <Leader>p :Buffers<CR>
nnoremap <Leader>f :Files<CR>

" git gutter toggle (Ctrl-L)
nnoremap <C-l> :GitGutterToggle<CR>

" goyo (distraction free writing) toggle (Ctrl-;)
nnoremap <Leader>; :Goyo<CR>

" gundo (,F5)
nnoremap <Leader>F5 :GundoToggle<CR>

" capital --> lowercase remappings
cabbrev X x
cabbrev W w

" move by visual line
nnoremap j gj
nnoremap k gk

" folding {{{
nnoremap <Leader>ea za
nnoremap <Leader>er zR

" }}}

" }}}

"""""""""""""
" FUNCTIONS " {{{ 
"""""""""""""

" enable limelight (automatically dim lines) when entering goyo mode
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" }}}

