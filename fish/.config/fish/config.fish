# custom prompt
function fish_prompt
    set_color -o blue
    echo -n (whoami)
    set_color normal
    echo -n "@"
    echo -n (hostname)
    echo -n " "
    if test (whoami) = root
        set_color -o red
        echo -n (prompt_pwd)
    else
        set_color -o white
        echo -n (prompt_pwd)
    end
    set_color normal
    echo -n ' % '
end

# source fzf (ctrl+t to search)
if test -f ~/.fzf/shell/key-bindings.fish
    source ~/.config/fish/fzf.fish
end
if test -f ~/.cargo/env
    source ~/.cargo/env
end

# add directories to path
if test -d ~/.apps/dart-sass
    set -x PATH $PATH ~/.apps/dart-sass
end
if test -d ~/.bin
    set -x PATH $PATH ~/.bin
end
if test -d ~/.local/bin
    set -x PATH $PATH ~/.local/bin
end
if test -d /usr/local/texlive/2019/bin/x86_64-linux
    set -x PATH $PATH /usr/local/texlive/2019/bin/x86_64-linux
end
if test -d /usr/local/go/bin
    set -x PATH $PATH /usr/local/go/bin
end
if test -d /usr/local/bin
    set -x PATH $PATH /usr/local/bin
end
if test -d ~/.cask/bin
    set -x PATH $PATH ~/.cask/bin
end

# set $EDITOR, $PAGER, $VISUAL, and $PATH
set -x EDITOR emacsclient -t
set -x PAGER less
set -x VISUAL emacsclient -t
