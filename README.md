# dotfiles

- `bspwm`: [bspwm](https://github.com/baskerville/bspwm) config
- `fish`: [fish shell](https://fishshell.com/) config & aliases
- `i3`: [i3](https://i3wm.org/) & [i3blocks](https://github.com/vivien/i3blocks) config
- `mpd`: [mpd](https://www.musicpd.org/) config
- `ncmpcpp`: [ncmpcpp](https://rybczak.net/ncmpcpp/) config
- `nvim`: [neovim](https://neovim.io/) config (init.vim)
- `polybar`: [polybar](https://github.com/jaagr/polybar) config (statusbar)
- `sway`: [sway](http://swaywm.org/) (i3 for wayland) and i3blocks config
- `xresources`: `~/.Xresources` file

### Usage

- Install: `stow <dir>`
- Uninstall: `stow -D <dir>`
